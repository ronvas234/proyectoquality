﻿using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using seleccionquality.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace seleccionquality.Servicios
{
    public class AlmacenarArchivos : IAlmacenadorArchivo
    {
        private readonly IWebHostEnvironment environment;
        private readonly IHttpContextAccessor httpaccesor;
        private readonly string contenedor = "vehiculo";
        public AlmacenarArchivos(IWebHostEnvironment env, IHttpContextAccessor _httpconaccesor)
        {
            environment = env;
            httpaccesor = _httpconaccesor;
        }
        public Task BorrarArchivo(string ruta, string contenedor)
        {
            if (ruta != null)
            {
                var nombrearchivo = Path.GetFileName(ruta);
                string directorio = Path.Combine(environment.WebRootPath, contenedor, nombrearchivo);
                if (File.Exists(directorio))
                {
                    File.Delete(directorio);
                }
            }
            return Task.FromResult(0);
        }

        public async Task<IArchivos> CargarArchivoExcel(byte[] contenido, string extension, string contenedor, string contentType)
        {
            var dir = await GuardarArchivo(contenido, extension, contenedor, contentType);
            IArchivos dtossubir= new IArchivos();
            dtossubir.listveh = new List<IEVehiculos>();
            var nombrearchivo = Path.GetFileName(dir);
            string directorio = Path.Combine(environment.WebRootPath, contenedor, nombrearchivo);
            if (File.Exists(directorio))
            {
                try
                {
                    FileInfo existingFile = new FileInfo(directorio);
                    using (ExcelPackage package = new ExcelPackage(existingFile))
                    {
                        ExcelWorksheet ws = package.Workbook.Worksheets[1];
                        int rowCount = ws.Dimension.End.Row;
                        for (int row = 2; row <= rowCount; row++)
                        {
                            var re = 0;
                            IEVehiculos item = new IEVehiculos();
                                item.placa = ws.Cells["A" + row.ToString()].Value.ToString();
                                item.marca = ws.Cells["B" + row.ToString()].Value.ToString();
                                item.modelo = ws.Cells["C" + row.ToString()].Value.ToString();
                            if (!int.TryParse(ws.Cells["D" + row.ToString()].Value.ToString(), out re))
                            {
                                dtossubir.mensaje += "Error de dato invalido debe ser numérico en la Columna D linea " + row.ToString();
                            }
                            else
                            {
                                item.ano = ws.Cells["D" + row.ToString()].Value.ToString();
                            }
                            item.serialmotor = ws.Cells["E" + row.ToString()].Value.ToString();
                            item.tipovehiculo = ws.Cells["F" + row.ToString()].Value.ToString();
                            if (dtossubir.mensaje == "" || dtossubir.mensaje == null)
                            {
                                dtossubir.listveh.Add(item);
                                dtossubir.lineasaprobadas = dtossubir.lineasaprobadas + 1;
                            }
                            dtossubir.lineasprocesadas = dtossubir.lineasprocesadas + row;
                        }
                    }
                }
                catch (Exception ex )
                {
                    dtossubir.mensaje = ex.Message;
                    dtossubir.lineasaprobadas = "0";
                    dtossubir.lineasprocesadas = "0";
                }
            }
            return dtossubir;
        }

        public async Task<string> EditarArchivo(byte[] contenido, string extension, string contenedor, string ruta, string contentType)
        {
            await BorrarArchivo(ruta, contenedor);
            return await GuardarArchivo(contenido, extension, contenedor, contentType);
        }
        public async Task<string> GuardarArchivo(byte[] contenido, string extension, string contenedor, string contentType)
        {
            var nombrearchivo = $"{Guid.NewGuid()}{extension}";
            string folder = Path.Combine(environment.WebRootPath, contenedor);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            string ruta = Path.Combine(folder, nombrearchivo);
            await File.WriteAllBytesAsync(ruta, contenido);
            var urlActual = $"{httpaccesor.HttpContext.Request.Scheme}://{httpaccesor.HttpContext.Request.Host}";
            var urlBD = Path.Combine(urlActual, contenedor, nombrearchivo).Replace("\\","/");
            return urlBD;
        }
    }
}
