﻿using seleccionquality.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace seleccionquality.Servicios
{
    public interface IAlmacenadorArchivo
    {
        Task<string> EditarArchivo(byte[] contenido, string extension, string contenedor, string ruta, string contentType);
        Task BorrarArchivo(string ruta,string contenedor);
        Task<string> GuardarArchivo(byte[] contenido, string extension, string contenedor, string contentType);
        Task<IArchivos> CargarArchivoExcel(byte[] contenido, string extension, string contenedor, string contentType);
    }
}
