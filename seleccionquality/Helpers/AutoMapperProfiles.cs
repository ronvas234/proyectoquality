﻿using AutoMapper;
using seleccionquality.Entities;
using seleccionquality.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace seleccionquality.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<TVehiculos, IVehiculosDTOS>().ReverseMap();
            CreateMap<TVehiculos, ICVehiculosDTOS>().ReverseMap();
            CreateMap<TVehiculos, IEVehiculos>().ReverseMap();
        }
    }
}
