﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace seleccionquality.Validaciones
{
    public class TipoArchivoValidacion:ValidationAttribute
    {
        private readonly string[] tiposvalidos;
        public TipoArchivoValidacion(string[] _tiposValidos)
        {
            tiposvalidos = _tiposValidos;
        }
        public TipoArchivoValidacion(GrupoTipoArchivo grupoTipoArchivo)
        {
            if (grupoTipoArchivo == GrupoTipoArchivo.Imagenes)
            {
                tiposvalidos = new string[] {"image/jpeg","image/png","image/gif" };
            }
            if (grupoTipoArchivo == GrupoTipoArchivo.Excel)
            {
                tiposvalidos = new string[] { "application/vnd.ms-excel", "application/msexcel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/xls", "application/x-xls" };
            }
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }
            IFormFile formFile = value as IFormFile;
            if (formFile == null)
            {
                return ValidationResult.Success;
            }
            if (!tiposvalidos.Contains(formFile.ContentType))
            {
                return new ValidationResult($"El tipo de archivo debe ser los siguientes: { string.Join(", ",tiposvalidos)}");
            }
            return ValidationResult.Success;
        }
    }
}
