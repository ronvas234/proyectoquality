﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace seleccionquality.Entities
{
    public class TVehiculos
    {
        [Key]
        public int id { get; set; }
        public string placa { get; set; }
        public string marca { get; set; }
        public string modelo { get; set; }
        public string ano { get; set; }
        public string serialmotor { get; set; }
        public string tipovehiculo { get; set; }
        public DateTime fechaingreso { get; set; }
        public bool estatus { get; set; }
        public string imagen { get; set; }
        public string idempleado { get; set; }
        public double ingreso { get; set; }
    }
}
