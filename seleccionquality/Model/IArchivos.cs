﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace seleccionquality.Model
{
    public class IArchivos
    {
        public List<IEVehiculos> listveh { get; set; }
        public string mensaje { get; set; }
        public string lineasprocesadas { get; set; }
        public string lineasaprobadas { get; set; }
    }
}
