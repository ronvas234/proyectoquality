﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace seleccionquality.Model
{
    public class UserToken
    {
        public string token { get; set; }
        public DateTime expiracion { get; set; }
    }
}
