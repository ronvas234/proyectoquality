﻿using Microsoft.AspNetCore.Http;
using seleccionquality.Validaciones;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace seleccionquality.Model
{
    public class ICVehiculosDTOS
    {
        [Required(ErrorMessage = "Debe ingresar la placa")]
        [StringLength(20, ErrorMessage = "Debe ingresar máximo 20 caracteres")]
        public string placa { get; set; }
        [Required(ErrorMessage = "Debe ingresar la marca")]
        [StringLength(20, ErrorMessage = "Debe ingresar máximo 20 caracteres")]
        public string marca { get; set; }
        [Required(ErrorMessage = "Debe ingresar el modelo")]
        [StringLength(20, ErrorMessage = "Debe ingresar máximo 20 caracteres")]
        public string modelo { get; set; }
        [Required(ErrorMessage = "Debe ingresar el año del vehiculo")]
        [StringLength(4, ErrorMessage = "Debe ingresar máximo 4 caracteres")]
        [Phone]
        public string ano { get; set; }
        [Required(ErrorMessage = "Debe ingresar el serial del motor")]
        [StringLength(30, ErrorMessage = "Debe ingresar máximo 30 caracteres")]
        public string serialmotor { get; set; }
        [Required(ErrorMessage = "Debe ingresar el serial del motor")]
        [StringLength(20, ErrorMessage = "Debe ingresar máximo 20 caracteres")]
        public string tipovehiculo { get; set; }
        [TipoArchivoValidacion(grupoTipoArchivo: GrupoTipoArchivo.Imagenes)]
        public IFormFile farchivo { get; set; }
        public string imagen { get; set; }
    }
}
