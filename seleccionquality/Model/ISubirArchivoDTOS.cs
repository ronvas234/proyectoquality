﻿using Microsoft.AspNetCore.Http;
using seleccionquality.Validaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace seleccionquality.Model
{
    public class ISubirArchivoDTOS
    {
        [TipoArchivoValidacion(grupoTipoArchivo: GrupoTipoArchivo.Excel)]
        public IFormFile farchivo { get; set; }
    }
}
