﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace seleccionquality.Model
{
    public class IEVehiculos
    {
        public string placa { get; set; }
        public string marca { get; set; }
        public string modelo { get; set; }
        public string ano { get; set; }
        public string serialmotor { get; set; }
        public string tipovehiculo { get; set; }
    }
}
