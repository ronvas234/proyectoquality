﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using seleccionquality.Contexts;
using seleccionquality.Entities;
using seleccionquality.Model;
using seleccionquality.Servicios;

namespace seleccionquality.Controllers
{
    [Route("api/subirarch")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SubirArchController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IAlmacenadorArchivo almacenar;
        private readonly string contenedor2 = "archivos";
        public SubirArchController(ApplicationDbContext _context, IMapper _mapper,IAlmacenadorArchivo _alm)
        {
            context = _context;
            mapper = _mapper;
            almacenar = _alm;
        }
        [HttpPost]
        public async Task<ActionResult> PostSubirArchivo([FromForm] ISubirArchivoDTOS model)
        {
            var mens = "";
            if (model.farchivo != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    await model.farchivo.CopyToAsync(memoryStream);
                    double monto = 200000;
                    var contenido = memoryStream.ToArray();
                    var extension = Path.GetExtension(model.farchivo.FileName);
                    var nombrearchivo = Path.GetFileName(model.farchivo.FileName);
                    var valor = await almacenar.CargarArchivoExcel(contenido, extension, contenedor2, model.farchivo.ContentType);
                    if (valor.mensaje == "" || valor.mensaje == null)
                    {
                        foreach (var item in valor.listveh)
                        {
                            var entidad = await context.Vehiculos.FirstOrDefaultAsync(x => x.placa == item.placa);
                            if (entidad == null)
                            {
                                var dto = mapper.Map<TVehiculos>(item);
                                var dias = DateTime.Now.Day;
                                var porc = 0;
                                if (Convert.ToInt32(dto.ano) <= 1997)
                                {
                                    porc = porc + 20;
                                }
                                if ((dias % 2) == 0)
                                {
                                    porc = porc + 5;
                                }
                                if (porc > 0)
                                {
                                    monto = monto + monto * porc / 100;
                                }
                                dto.ingreso = monto;
                                dto.fechaingreso = DateTime.Now;
                                dto.estatus = true;
                                context.Add(dto);
                                await context.SaveChangesAsync();
                            }
                            else
                            {
                                entidad = mapper.Map(item, entidad);
                                entidad.fechaingreso = DateTime.Now;
                                await context.SaveChangesAsync();
                            }
                            mens = "";
                        }
                        var listquery = await context.Vehiculos.ToListAsync();
                        foreach (var item in listquery)
                        {
                            var est = valor.listveh.FirstOrDefault(x => x.placa == item.placa);
                            if (est == null)
                            {
                                item.estatus = false;
                                item.fechaingreso = DateTime.Now;
                                await context.SaveChangesAsync();
                            }
                        }
                    }
                    else
                    {
                        mens = valor.mensaje;
                    }
                }
            }
            else
            {
                mens = "Debe enviar un archivo";
            }
            if (mens == "")
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
