﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using seleccionquality.Contexts;
using seleccionquality.Entities;
using seleccionquality.Model;
using seleccionquality.Servicios;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace seleccionquality.Controllers
{
    [ApiController]
    [Route("api/Vehiculos")]
    [Authorize(AuthenticationSchemes =JwtBearerDefaults.AuthenticationScheme)]
    public class VehiculosController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IAlmacenadorArchivo almacenar;
        private readonly string contenedor = "imagenes";
        public VehiculosController(ApplicationDbContext _context, IMapper _mapper, IAlmacenadorArchivo _alm)
        {
            context = _context;
            mapper = _mapper;
            almacenar = _alm;
        }
        [HttpGet]
        public async Task<ActionResult<List<IVehiculosDTOS>>> Get()
        {
            var entidades = await context.Vehiculos.ToListAsync();
            var dtos = mapper.Map<List<IVehiculosDTOS>>(entidades);
            return dtos;
        }
        [HttpGet("{id:int}", Name = "ObtenerVehiculos")]
        public async Task<ActionResult<IVehiculosDTOS>> Get(int id)
        {
            var entidades = await context.Vehiculos.Where(x => x.id == id).FirstOrDefaultAsync();
            if (entidades == null)
            {
                return NotFound();
            }
            else
            {
                var dtos = mapper.Map<IVehiculosDTOS>(entidades);
                return dtos;
            }
        }
        [HttpPost]
        public async Task<ActionResult<IVehiculosDTOS>> Post([FromForm] ICVehiculosDTOS model)
        {
            var re = 0;
            if (int.TryParse(model.ano, out re))
            {
                var entidad = mapper.Map<TVehiculos>(model);
                double monto = 200000;
                var dias = DateTime.Now.Day;
                var porc = 0;
                if (Convert.ToInt32(entidad.ano) <= 1997)
                {
                    porc = porc + 20;
                }
                if ((dias % 2) == 0)
                {
                    porc = porc + 5;
                }
                if (porc > 0)
                {
                    monto = monto + monto * porc / 100;
                }
                entidad.ingreso = monto;
                entidad.fechaingreso = DateTime.Now;
                entidad.estatus = true;
                if (model.farchivo != null)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await model.farchivo.CopyToAsync(memoryStream);
                        var contenido = memoryStream.ToArray();
                        var extension = Path.GetExtension(model.farchivo.FileName);
                        entidad.imagen = await almacenar.GuardarArchivo(contenido, extension, contenedor, model.farchivo.ContentType);
                    }
                }
                context.Add(entidad);
                await context.SaveChangesAsync();
                var dtos = mapper.Map<IVehiculosDTOS>(entidad);
                return new CreatedAtRouteResult("ObtenerVehiculos", new { id = dtos.id }, dtos);
            }
            else
            {
                return NotFound();
            }
        }
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromForm] ICVehiculosDTOS model)
        {
            var entidad = await context.Vehiculos.FirstOrDefaultAsync(x => x.id == id);
            if (entidad == null)
            {
                return NotFound();
            }
            entidad = mapper.Map(model, entidad);
            if (model.farchivo != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    await model.farchivo.CopyToAsync(memoryStream);
                    var contenido = memoryStream.ToArray();
                    var extension = Path.GetExtension(model.farchivo.FileName);
                    entidad.imagen = await almacenar.EditarArchivo(contenido, extension, contenedor,model.imagen, model.farchivo.ContentType);
                }
            }
            await context.SaveChangesAsync();
            return NoContent();
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var entidad = await context.Vehiculos.AnyAsync(x => x.id == id);
            if (!entidad)
            {
                return NotFound();
            }
            else
            {
                context.Remove(new TVehiculos() { id = id });
                await context.SaveChangesAsync();
                return NoContent();
            }
        }
    }
}
